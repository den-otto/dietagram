<?php //>

class D extends CVarDumper 
{
    /**
     * Displays a variable.
     * This method achieves the similar functionality as var_dump and print_r
     * but is more robust when handling complex objects such as Yii controllers.
     * @param mixed variable to be dumped
     * @param integer maximum depth that the dumper should go into the variable. Defaults to 10.
     * @param boolean whether the result should be syntax-highlighted
     */
    public static function dump($var, $depth = 15, $highlight = true)
	{
        echo self::dumpAsString($var, $depth, $highlight);
    }
	
	/**
	* Print value
	*
	* @param mixed	$v - value to output
	* @param string	$caption - customized caption for debug window
	* @param bool	$stop_script - flag to stop script at the end of output debag info
	*/
	public static function _d($v, $stop_script = false, $only_for_admin = false, $caption = false)
	{
		if($only_for_admin)
		{
			if(Yii::app()->user->getName() !== "dimas.xbs@gmail.com") return false;
		}
		if (is_bool($v)) $v = $v? "true" : "false";
		if (is_null($v)) $v = "null";
		if (!$caption) $caption = 'Debug info';
		print "<div style='background-color:#a9b8cd; color:#fff; padding: 2px 10px; font-weight:bold; margin: 0'>{$caption}:</div>";
		print "<xmp style='border:1px solid #555;background-color:#eff3f6;padding:10px;margin:0'>";
		print_r($v);
		print "</xmp>";
		if ($stop_script) die();
	}
        
        public static function _da($array, $stop_script = false, $only_for_admin = false, $caption = false){
            foreach($array as $index=>$el){
                d::_d($index, $stop_script = false, $only_for_admin = false, $caption = false);
                d::_d($el->attributes, $stop_script = false, $only_for_admin = false, $caption = false);
            }
        }
}