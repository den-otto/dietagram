<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Связаться с нами';
?>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

    <section id="content">
        <div class="container_12">
            <div class="grid_12">
                <div class="box-shadow">
                    <div class="wrap block-2">

                        <div class="col-3">
                            <h2><span class="color-1">Найти</span> нас</h2>
                            <div class="map img-border">
                                <iframe src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Brooklyn,+New+York,+NY,+United+States&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=61.282355,146.513672&amp;ie=UTF8&amp;hq=&amp;hnear=Brooklyn,+Kings,+New+York&amp;ll=40.649974,-73.950005&amp;spn=0.01628,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
                            </div>
                            <dl>
                                <dt class="color-1"><strong>Чкалова 35,<br>Минск, Беларусь.</strong></dt>
                                <dd><span>Freephone:</span>+1 800 559 6580</dd>
                                <dd><span>Telephone:</span>+1 800 603 6035</dd>
                                <dd><span>Fax:</span>+1 800 889 9898</dd>
                                <dd><span>E-mail:</span><a href="#" class="link">mail@demolink.org</a></dd>
                            </dl>
                        </div>

                        <div class="col-4">
                            <h2><span class="color-1">Связаться</span> с нами</h2>
                            <p class="note">Поля, помеченные <span class="required">*</span>, обязательны для заполнения.</p>

                            <div class="form">
                                <?php $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'contact-form',
                                    'enableClientValidation'=>true,
                                    'clientOptions'=>array(
                                        'validateOnSubmit'=>true,
                                    ),
                                )); ?>

                                <?php echo $form->errorSummary($model); ?>

                                <div class="row">
                                    <?php echo $form->textField($model,'name', array('placeholder' => 'Имя')); ?>
                                </div>

                                <div class="row">
                                    <?php echo $form->textField($model,'email', array('placeholder' => 'Email')); ?>
                                </div>

                                <div class="row">
                                    <?php echo $form->textArea($model,'body', array(
                                                    'rows'=>6,
                                                    'cols'=>50,
                                                    'placeholder' => 'Сообщение',
                                                    )
                                                );
                                    ?>
                                </div>

                                <?php if(CCaptcha::checkRequirements()): ?>
                                    <div class="row">
                                        <?php echo $form->textField($model,'verifyCode', array(
                                                        'placeholder' => 'Код подтверждения',
                                                        )
                                                    );
                                        ?>
                                        <?php $this->widget('CCaptcha'); ?>
                                    </div>
                                <?php endif; ?>

                                <div class="row-button">
                                    <?php echo CHtml::submitButton('Отправить', array('class' => 'contactBtn' )); ?>
                                </div>

                                <?php $this->endWidget(); ?>

                            </div><!-- form -->

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </section>

<?php endif; ?>