<?php
/**
 * this is view for application page
 */

$this->pageTitle=Yii::app()->name;
?>

<section id="content">
    <div class="container_12">
        <div class="grid_12">
            <div class="box-shadow">
                <div class="wrap block-2">


<!--       Калькулятор калорий             -->
                        <h2 class="p3"><span class="color-1">Калькулятор</span> калорий</h2>
                        <p class="p2"><strong>Описание</strong></p>
                        <p class="p5">Полностью на русском языке. Простой калькулятор калорий (без учёта БЖУ). Контроль
                            веса (график). Поиск друзей по диете. Большая база продуктов. Вы можете дополнить её своими
                            собственными. Калькуляторы BMI и BMR и анализатор вашего рациона позволяют рассчитать
                            необходимое число калорий для улучшения Вашей фигуры без вреда для здоровья.
                        </p>
                        <div class="pag">
                            <div class="img-pags">
                                <ul>
                                   <img class="screen" src="/img/screen/calc1.jpg" alt="">
                                   <img class="screen" src="/img/screen/calc2.jpg" alt="">
                                   <img class="screen" src="/img/screen/calc3.jpg" alt="">
                                </ul>
                            </div>
                        </div>

                        <div class="market-button">

                            <a href="https://play.google.com/store/apps/details?id=bulat.diet.helper_ru" class="button top-6" target="_blank">Установить</a>
                            <img class="android-icons" src="/img/icons/android.png" alt="icon"/>
                        </div>

                        <hr/>

<!--       Калькулятор калорий  плюс           -->

                        <h2 class="p3"><span class="color-1">Калькулятор</span> калорий <span class="color-1">PLUS</span></h2>
                        <p class="p2"><strong>Описание</strong></p>
                        <p class="p5">Полностью на русском языке. Простой калькулятор калорий (без учёта БЖУ). Контроль
                            веса (график). Поиск друзей по диете. Большая база продуктов. Вы можете дополнить её своими
                            собственными. Калькуляторы BMI и BMR и анализатор вашего рациона позволяют рассчитать
                            необходимое число калорий для улучшения Вашей фигуры без вреда для здоровья.
                        </p>
                        <div class="pag">
                            <div class="img-pags">
                                <ul>
                                    <img class="screen" src="/img/screen/calcplus1.jpg" alt="">
                                    <img class="screen" src="/img/screen/calcplus2.jpg" alt="">
                                    <img class="screen" src="/img/screen/calcplus3.jpg" alt="">
                                </ul>
                            </div>
                        </div>

                        <div class="market-button">
                            <a href="https://play.google.com/store/apps/details?id=bulat.diet.helper_plus" class="button top-6" target="_blank">Установить</a>
                            <img class="android-icons" src="/img/icons/android.png" alt="icon"/>
                        </div>

                        <hr/>
<!--       Калькулятор калорий  спорт           -->

                    <h2 class="p3"><span class="color-1">Калькулятор</span> калорий <span class="color-1">SPORT</span></h2>
                    <p class="p2"><strong>Описание</strong></p>
                    <p class="p5">Калькулятор Калорий SPORT, разработан для людей активно занимающихся спортом.
                        Полностью на русском(и оформление и база продуктов). Ежедневный учёт потребления калорий,
                        белков, жиров и углеводов. Будильники на каждый приём пищи. Возможность составлять шаблоны
                        питания. Контроль веса и других параметров тела. Большая база продуктов. Вы можете дополнить
                        её своими собственными. Калькуляторы BMI и BMR позволяют рассчитать необходимое число калорий
                        для улучшения Вашей фигуры без вреда для здоровья.
                    </p>
                    <div class="pag">
                        <div class="img-pags">
                            <ul>
                                <img class="screen" src="/img/screen/calsport1.jpg" alt="">
                                <img class="screen" src="/img/screen/calsport2.jpg" alt="">
                                <img class="screen" src="/img/screen/calsport3.jpg" alt="">
                            </ul>
                        </div>
                    </div>

                    <div class="market-button">
                        <a href="https://play.google.com/store/apps/details?id=bulat.diet.helper_sport" class="button top-6" target="_blank">Установить</a>
                        <img class="android-icons" src="/img/icons/android.png" alt="icon"/>
                    </div>

                    <hr/>

<!--       Калькулятор калорий  ios PRO           -->

                    <h2 class="p3"><span class="color-1">Калькулятор</span> калорий <span class="color-1">PRO</span></h2>
                    <p class="p2"><strong>Описание</strong></p>
                    <p class="p5">Калькулятор Калорий SPORT, разработан для людей активно занимающихся спортом.
                        Полностью на русском(и оформление и база продуктов). Ежедневный учёт потребления калорий,
                        белков, жиров и углеводов. Будильники на каждый приём пищи. Возможность составлять шаблоны
                        питания. Контроль веса и других параметров тела. Большая база продуктов. Вы можете дополнить
                        её своими собственными. Калькуляторы BMI и BMR позволяют рассчитать необходимое число калорий
                        для улучшения Вашей фигуры без вреда для здоровья.
                    </p>
                    <div class="pag">
                        <div class="img-pags">
                            <ul>
                                <img class="screen" src="/img/screen/calcios1.jpg" alt="">
                                <img class="screen" src="/img/screen/calcios2.jpg" alt="">
                                <img class="screen" src="/img/screen/calcios3.jpg" alt="">
                            </ul>
                        </div>
                    </div>

                    <div class="market-button">
                        <a href="https://itunes.apple.com/ru/app/kal-kulator-kalorij-pro/id718931907?mt=8" class="button top-6" target="_blank">Установить</a>
                        <img class="android-icons" src="/img/icons/ios.png" alt="icon"/>
                    </div>

                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>