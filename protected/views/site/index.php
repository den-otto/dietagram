<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<script type="application/javascript">
    // график
    <!--    google.load('visualization', '1', {'packages':['corechart']});-->
    <!--    google.setOnLoadCallback(drawChart);-->
    <!--    function drawChart() {-->
    <!--        var jsonChartData = $.ajax({-->
    <!--            type: "POST",-->
    <!--            url: "--><?php //echo Yii::app()->createUrl('ajax/GetIndexChartData');?><!--",-->
    <!--            dataType: "json",-->
    <!--            async: false-->
    <!--        }).responseText;-->
    <!---->
    <!--        var options = {-->
    //title: 'Lengths of dinosaurs, in meters',
    <!--            legend: { position: 'none' },-->
    <!--            width: 350,-->
    <!--            height: 400-->
    <!--        };-->
    <!---->
    <!--        var data = new google.visualization.DataTable(jsonChartData);-->
    <!---->
    <!--        var chart = new google.visualization.ColumnChart(document.getElementById('chart'));-->
    <!--        chart.draw(data, options);-->
    <!--    }-->


    $(document).ready(function(){

        function setTableData(val){
            var opts = {
                lines: 11, // The number of lines to draw
                length: 37, // The length of each line
                width: 9, // The line thickness
                radius: 35, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 42, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#000', // #rgb or #rrggbb or array of colors
                speed: 2.1, // Rounds per second
                trail: 54, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: '50%', // Top position relative to parent
                left: '50%' // Left position relative to parent
            };

            var target = document.getElementById('loading');
            var spinner = new Spinner(opts).spin(target);

            if (undefined != val) {
                inputSearch = val;
            } else {
                inputSearch = $(".input_search").val();
            }

            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createUrl('ajax/GetIndexTableData'); ?>",
                data: "inputSearchData="+inputSearch,
                success: function(data){
                    spinner.stop();
                    var tableData = JSON.parse(data);
                    var count = tableData['dishes'].length;
                    if (count == 0) {
//                            alert('По данному запросу ничего не найдено! Попробуйте еще раз.');
                        //break;
                        $('#search-error').modal();
                    }
                    else {
                        var row = '';
                        for(var i = 0; i < count; i++) {
                            if (i <= 9) {
                                row += '<tr><td>'+tableData['dishes'][i].name+'</td><td>'+tableData['dishes'][i].protein+'</td><td>'+tableData['dishes'][i].fat+'</td><td>'+tableData['dishes'][i].carbon+'</td><td>'+tableData['dishes'][i].caloric+'</td></tr>';
                            } else { break;}
                        }
                        $("tbody tr").remove();
                        $('#background-image tbody').html(row);
                    }
                }
            });
        };


        $(".search_button").on('click', setTableData);
        $(".input_search").keypress(function(e){
            if(e.keyCode==13){
                setTableData();
            }
        });


        $("#input_search").autocomplete({
            source: "/ajax/GetAutocompleteData",
            select: function(event, ui) {
                setTableData(ui.item.value);
            },
            minLength: 2,
            html: true

        });

    });

</script>

<section id="content">

    <!--  Error message for search  -->
<!--    <div id="search-error" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="Ничего не найдено!" aria-hidden="true">-->
<!--        <div class="modal-dialog modal-sm">-->
<!--            <div class="modal-content">-->
<!--                <div class="modal-header">-->
<!--                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
<!--                    <h4>Ничего не найдено.</h4>-->
<!--                </div>-->
<!--                <div class="modal-body">-->
<!--                    <p>По данному запросу ничего не найдено! Попробуйте еще раз.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

    <div class="container_12">
        <div class="grid_12">
            <div class="slider">

                <div class="form-search">
                    <form id="form-search" method="post" onsubmit="javascript: $('.search_button').trigger('click'); return false;" >
                        <input type="text" id="input_search" class="input_search"  placeholder="Поиск продукта или активности " />
                        <a href="#" class="search_button">Поиск</a>
                    </form>
                </div>

                <img src="/images/index_img.jpg" alt="" />

                <?php if (Yii::app()->user->isGuest) : ?>
                    <div class="reg-form">
                        <h2 class="p3-index"><span class="color-1">Вход</span> в личный кабинет</h2>

                        <?php $this->renderPartial('_login', array('loginModel' => $loginModel));?>

                    </div>
                <?php endif?>

            </div>
        </div>

        <div class="grid_12 top-1">
            <div class="box-shadow">
                <div class="wrap block-2">



                    <div class="col-1">

                        <div class="wrap box-1">

                            <div id="loading"></div>

                            <table id="background-image">
                                <thead>
                                <tr>
                                    <th scope="col">Название</th>
                                    <th scope="col">Белки</th>
                                    <th scope="col">Жиры</th>
                                    <th scope="col">Углеводы</th>
                                    <th scope="col">Ккал/100&nbsp;гр</th>
                                </tr>
                                </thead>
                                <tbody id="index_table_body">
                                <tr>
                                    <td>Огурец</td>
                                    <td>0,8</td>
                                    <td>0,1</td>
                                    <td>3</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>Яблоко</td>
                                    <td>0,4</td>
                                    <td>0,4</td>
                                    <td>9,8</td>
                                    <td>47</td>
                                </tr>
                                <tr>
                                    <td>Кофе без сахара</td>
                                    <td>0.04</td>
                                    <td>0.05</td>
                                    <td>0</td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td>Бананы</td>
                                    <td>1.5</td>
                                    <td>0.1</td>
                                    <td>21.8</td>
                                    <td>89</td>
                                </tr>
                                <tr>
                                    <td>Клубника</td>
                                    <td>0,6</td>
                                    <td>0,4</td>
                                    <td>7</td>
                                    <td>30</td>
                                </tr>
                                <tr>
                                    <td>Молоко 2.5%</td>
                                    <td>2,8</td>
                                    <td>2,5</td>
                                    <td>4,7</td>
                                    <td>52</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-2">
                        <h2 class="p3-index"><span class="color-1">Топ 5</span> продуктов за сутки</h2>
                        <div id = "chart"></div>

                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>

