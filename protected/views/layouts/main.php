<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="utf-8">

    <link rel="shortcut icon" href="/img/icons/favicon.png" type="image/png">

    <link rel="stylesheet" type="text/css" media="screen" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/style.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/grid_12.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/slider.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/table.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/demo.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/js/libs/jqui/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/css/zabuto_calendar.min.css">

<!--    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/js/libs/jqui/jquery-ui.min.js"></script>

    <script src="/js/libs/zabuto_calendar.js"></script>
    <script src="/js/libs/spin.min.js"></script>



    <script type="text/javascript" src="https://www.google.com/jsapi"></script>





    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/js/html5.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="/css/ie.css">
    <![endif]-->



</head>
<body>

<div class="main">
    <div class="bg-img"></div>
    <!--==============================header=================================-->
    <header>
        <h1><a href="<?php echo Yii::app()->createUrl('site/index');?>">Dieta <strong>Gram.</strong></a></h1>
        <nav>
            <div class="social-icons">
                <a href="https://www.facebook.com/367309416714909" class="icon-2" target="_blank"></a>
                <a href="https://vk.com/diethelper" class="icon-1" target="_blank"></a>
            </div>
            <ul class="menu">
                <li class="current"><a href="<?php echo Yii::app()->createUrl('site/index'); ?>">Главная</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('site/application');?>">Мобильные приложения</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('site/contact');?>">Контакты</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('account/index');?>">Кабинет</a></li>
            </ul>
        </nav>
    </header>
    <!--==============================content================================-->
    <?php echo $content; ?>

    <!--==============================footer=================================-->
</div>
<footer>
    <p>© 2014 Dietagram</p>

    <div style="margin: 20px auto; width: 950px">
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>

        <!-- VK Widget -->
        <div id="vk_groups" style="float: left;"></div>
        <script type="text/javascript">
            //                VK.Widgets.Group("vk_groups", {mode: 0, width: "470", height: "300", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 47759767);
        </script>

        <div id="fb-root"></div>

        <div class="fb-like-box" data-href="https://www.facebook.com/KalkulatorKalorij" data-width="470" data-height="300" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
        <script>
            //            (function(d, s, id) {
            //                var js, fjs = d.getElementsByTagName(s)[0];
            //                if (d.getElementById(id)) return;
            //                js = d.createElement(s); js.id = id;
            //                js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.0";
            //                fjs.parentNode.insertBefore(js, fjs);
            //            }(document, 'script', 'facebook-jssdk'));
        </script>
    </div>

</footer>

</body>
</html>