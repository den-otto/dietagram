
    <?php $form = $this->beginWidget('CActiveForm', array(
            'htmlOptions'=>array(
                'class'=>'form-horizontal',
                'role' => 'form',

            ),
        )
    ); ?>

        <div class="form-group">
            <?php echo $form->label($data,'Продукт', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-6">
                <?php echo $form->textField($data,'name', array('class' => 'form-control', 'id' => 'input_search')) ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->label($data,'Вес ', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-6">
                <?php echo $form->textField($data,'dishweight', array('class' => 'form-control', 'id' => 'input_search')) ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->label($data,'Время', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-6">
                <?php echo $form->dropDownList($data,'daytime',
                    array(
                        UserData::BREAKFAST => 'Завтрак',
                        UserData::SECOND_BREAKFAST => 'Второй завтрак',
                        UserData::LUNCH => 'Обед',
                        UserData::AFTERNOON_SNACK => 'Полдник',
                        UserData::EVENING_MEAL => 'Ужин',
                    ),
                    $htmlOptions = array('class' => 'form-control')
                ) ?>
            </div>
        </div>



        <div class="modal-footer">
            <?php echo CHtml::ajaxSubmitButton('Добавить',
                '/account/UpdateApplication',
                array(
//                    'update' => '#application',
//                    'beforeSend' => 'js: function(){
//
//                                        }',
//                    'complete' => 'js: function(){}',
                    'success' => 'js: function(data){
                                        $("#application").html(data);
                                        $(".add_meal_modal").modal("hide");
                                    }',
                ),
                array(
                    'class' => 'btn btn-primary btn-sm'
                )
            ); ?>
        </div>

    <?php $this->endWidget(); ?>



