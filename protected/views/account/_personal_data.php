<?php
/**
 * личные параметры пользователья (рост, вес и тд)
 */

$form = $this->beginWidget('CActiveForm', array(
        'htmlOptions'=>array(
            'class'=>'form-horizontal',
            'role' => 'form',
            'name' => 'personalData'
        ),
    )

); ?>


<div class="form-group">
    <?php echo $form->label($data,'Вес', array('class' => 'col-sm-12 control-label')); ?>
    <?php echo $form->textField($data,'weight', array('class' => 'form-control')) ?>
</div>

<div class="form-group">
    <?php echo $form->label($data,'Возраст', array('class' => 'col-sm-12 control-label')); ?>
    <?php echo $form->textField($data,'age', array('class' => 'form-control')) ?>
</div>

<div class="form-group">
    <?php echo $form->label($data, 'Рост', array('class' => 'col-sm-12 control-label')); ?>
    <?php echo $form->textField($data,'hight', array('class' => 'form-control')) ?>
</div>

<div class="form-group">
    <?php echo $form->label($data,'Уровень активности', array('class' => 'col-sm-12 control-label')); ?>
    <?php echo $form->dropDownList($data,'activitylvl', array('1' => 'Сидячий', '2' => 'Подвижный'), $htmlOptions = array('class' => 'form-control')) // todo:  ?>
</div>

<div class="form-group">
    <?php echo $form->label($data,'Режим питания', array('class' => 'col-sm-12 control-label')); ?>
    <?php echo $form->dropDownList($data,'name', array('1' => 'Поддержание', '2' => 'Похудение'), $htmlOptions = array('class' => 'form-control')) // todo:  ?>

</div>

<div class="modal-footer">
    <?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-primary btn-sm')); ?>
</div>
<?php $this->endWidget(); ?>


