<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<!-- Bootstrap -->
<link href="/css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="/css/bootstrap/js/bootstrap.min.js"></script>


<section id="content">
    <div class="container_12">
        <div class="grid_12 top-1">
            <div class="box-shadow">
                <div class="wrap block-2">

                    <nav class="navbar navbar-default" role="navigation" style="width: 866px;">
                        <div class="container-fluid">
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                                <ul class="nav navbar-nav">
                                    <li><a href="#"  data-toggle="modal"  data-target=".add_meal_modal">Новый прием пищи</a></li>
                                </ul>

                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Дополнительно<span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">One more separated link</a></li>
                                        </ul>
                                    </li>
                                </ul>

                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>

                    <div id="loading"></div>
                    <div class="application" id="application">

                        <?php $this->renderPartial('_application', array('currentDay' => $currentDay));?>
                    </div>

                    <div class="right-sidebar">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            Календарь
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div id="calendar"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            Личные параметры
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <?php $this->renderPartial('_personal_data', array('data' => $personalData))?>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            Что-то еще
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Тут можно вставить график
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade add_meal_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Новый прием пищи</h4>
            </div>
            <div class="modal-body">
                <?php $this->renderPartial('_add_meal', array('data' => $meal)) ?>
            </div>
        </div>
    </div>
</div>


<script type="application/javascript">
    $(document).ready(function () {

        $("#calendar").zabuto_calendar({
            cell_border: true,
            today: true,
            show_days: true,
            weekstartson: 1,
            language: 'ru'
        });

        $("#input_search").autocomplete({
            source: "/ajax/GetAutocompleteData",
            minLength: 2,
            html: true
        });

    });
</script>