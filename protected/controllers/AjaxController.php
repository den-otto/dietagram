<?php

class AjaxController extends Controller
{
    /**
     * @throws CHttpException
     */
    public function actionGetIndexTableData()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException('404. Запрошенная страница не найдена.');
            Yii::app()->end();
        }

        $queryData = Yii::app()->request->getPost('inputSearchData');

        $criteria = new CDbCriteria();
        $criteria->order = 'name ASC';
        $criteria->limit = 10;
        $criteria->condition = 'name LIKE :name';
        $criteria->params = array(':name' => $queryData . '%');

        $data = Base::model()->findAll($criteria);

        $tableData = array();
        $i = 0;

        foreach ($data as $item) {
            $tableData[$i]['name'] = $item->name;
            $tableData[$i]['caloric'] = $item->caloric;
            $tableData[$i]['fat'] = $item->fat;
            $tableData[$i]['carbon'] = $item->carbon;
            $tableData[$i]['protein'] = $item->protein;
            $i++;
        }

        $tableData = json_encode($tableData);

        echo '{"dishes":' . $tableData . '}';    }

    /**
     * @throws CHttpException
     */
    public function actionGetIndexChartData()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException('404. Запрошенная страница не найдена.');
            Yii::app()->end();
        }

        $chartData  = Yii::app()->curl->get('http://www.calorycman.net/top.php?type=1410235200000');
        $chartData = substr($chartData, 13);
        $chartArrayData = json_decode($chartData, true);
        $rows = '{
                "cols": [
                    {"id":"","label":"Topping","pattern":"","type":"string"},
                    {"id":"","label":"Вес, кг","pattern":"","type":"number"}
                ],
                "rows" : [';

        foreach($chartArrayData['top'] as $item) {
            $rows .= '{"c":[{"v":"'.$item["name"].'", "f":null}, {"v":'.$item["num"].', "f":null}]},';
            //echo $item['name'];
        }
        /*$chartData = '{
          "cols": [
                {"id":"","label":"Topping","pattern":"","type":"string"},
                {"id":"","label":"Вес, кг","pattern":"","type":"number"}
              ],
          "rows": [
                {"c":[{"v":"Mushrooms","f":null},{"v":3,"f":null}]},
                {"c":[{"v":"Onions","f":null},{"v":1,"f":null}]},
                {"c":[{"v":"Olives","f":null},{"v":1,"f":null}]},
                {"c":[{"v":"Zucchini","f":null},{"v":1,"f":null}]},
                {"c":[{"v":"Pepperoni","f":null},{"v":2,"f":null}]}
              ]
        }';*/

        echo $rows.']}';
    }

    public function actionGetAutocompleteData($term)
    {
//        if (!Yii::app()->request->isAjaxRequest) {
//            throw new CHttpException('404. Запрошенная страница не найдена.');
//            Yii::app()->end();
//        }

//        $q = Yii::app()->request->getPost('inputSearchData');

        $criteria = new CDbCriteria();
        $criteria->order = 'name ASC';
        $criteria->select = 'name';
        $criteria->condition = 'name LIKE :name';
        $criteria->limit = 10;
        $criteria->params = array(':name' => $term . '%');

        $data = Base::model()->findAll($criteria);

        $name = array();
        foreach ($data as $item) {
            $name[] = $item['name'];
        }

        $name = json_encode($name);

        echo $name;

    }

    public function actionSearchApi($term)
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'name ASC';
        $criteria->limit = 10;
        $criteria->condition = 'name LIKE :name';
        $criteria->params = array(':name' => $term . '%');

        $data = Base::model()->findAll($criteria);

        $tableData = array();
        $i = 0;

        foreach ($data as $item) {
            $tableData[$i]['name'] = $item->name;
            $tableData[$i]['caloric'] = $item->caloric;
            $tableData[$i]['fat'] = $item->fat;
            $tableData[$i]['carbon'] = $item->carbon;
            $tableData[$i]['protein'] = $item->protein;
            $i++;
        }

        $tableData = json_encode($tableData);

        echo '{"dishes":' . $tableData . '}';    }

}