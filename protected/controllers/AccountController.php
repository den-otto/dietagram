<?php

/**
 *  user personal account
**/
class AccountController extends CController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array('index', 'UpdateApplication'),
                'users'=>array('?'),
            ),
            array('allow',
                'actions'=>array('index', 'UpdateApplication'),
                'users'=>array('@'),
            ),
            array('deny',
                'actions'=>array('index', 'UpdateApplication'),
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $meal = new UserData();
        $personalData = $this->loadUser();
        $currentDay = $this->loadCurrentDay();


        $this->render('index', array('meal' => $meal, 'personalData' => $personalData, 'currentDay' => $currentDay));

    }

    public function actionUpdateApplication()
    {
        if (isset($_POST['UserData'])) {
            $mealData = $_POST['UserData'];
            $this->saveMeal($mealData);

            $currentDay = $this->loadCurrentDay();

            $data = $this->renderPartial('_application', array('currentDay' => $currentDay), false, true);

            echo $data;

        }
    }

    private function loadUser()
    {
        $user = UserRegistration::model()->findByAttributes(array('id' => Yii::app()->user->id));

        return $user;
    }

    private function saveMeal($mealData)
    {
        $userData = new UserData();

        $userData->name = $mealData['name'];
        $userData->dishweight = +$mealData['dishweight'];
        $userData->daytime = +$mealData['daytime'];
        $userData->userid = +Yii::app()->user->id;
        $userData->datelong = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

        $userData->save();

    }

    private function loadCurrentDay()
    {
        $currentDate = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

        $criteria = new CDbCriteria();
        $criteria->condition = 'userid=:userid AND datelong = :currentDate';
        $criteria->params = array(':userid' => Yii::app()->user->id, ':currentDate' => $currentDate);

        $data = UserData::model()->findAll($criteria);

        $dataArray = array();
        foreach ($data as $record) {
            $dataArray[$record->daytime][] = array(
                'name' => $record->name,
                'dishweight' => $record->dishweight,
                'dishweight' => $record->dishweight,
            );
        }

        return $dataArray;
    }



}