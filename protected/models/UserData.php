<?php

/**
 * This is the model class for table "user_data".
 *
 * The followings are the available columns in table 'user_data':
 * @property integer $id
 * @property integer $bodyweight
 * @property string $name
 * @property integer $daytime
 * @property integer $calorisity
 * @property integer $calorie
 * @property integer $category
 * @property integer $dishweight
 * @property string $datetext
 * @property integer $datelong
 * @property integer $fat
 * @property integer $carbon
 * @property integer $protein
 * @property integer $fatabs
 * @property integer $carbonabs
 * @property integer $proteinabs
 * @property string $type
 * @property integer $hh
 * @property integer $mm
 * @property integer $issport
 * @property integer $chest
 * @property integer $pelvis
 * @property integer $biceps
 * @property integer $shin
 * @property integer $neck
 * @property integer $waist
 * @property integer $forearm
 * @property integer $hip
 * @property integer $userid
 * @property integer $activeflag
 */
class UserData extends CActiveRecord
{
    const BREAKFAST = 0;
    const SECOND_BREAKFAST = 1;
    const LUNCH = 2;
    const AFTERNOON_SNACK = 3;
    const EVENING_MEAL = 4;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_data';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('bodyweight, name, daytime, calorisity, calorie, category, dishweight, datetext, datelong, fat, carbon, protein, fatabs, carbonabs, proteinabs, type, hh, mm, issport, chest, pelvis, biceps, shin, neck, waist, forearm, hip, userid', 'required'),
//			array('bodyweight, daytime, calorisity, calorie, category, dishweight, datelong, fat, carbon, protein, fatabs, carbonabs, proteinabs, hh, mm, issport, chest, pelvis, biceps, shin, neck, waist, forearm, hip, userid, activeflag', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, bodyweight, name, daytime, calorisity, calorie, category, dishweight, datetext, datelong, fat, carbon, protein, fatabs, carbonabs, proteinabs, type, hh, mm, issport, chest, pelvis, biceps, shin, neck, waist, forearm, hip, userid, activeflag', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'bodyweight' => 'Bodyweight',
			'name' => 'Name',
			'daytime' => 'Daytime',
			'calorisity' => 'Calorisity',
			'calorie' => 'Calorie',
			'category' => 'Category',
			'dishweight' => 'Dishweight',
			'datetext' => 'Datetext',
			'datelong' => 'Datelong',
			'fat' => 'Fat',
			'carbon' => 'Carbon',
			'protein' => 'Protein',
			'fatabs' => 'Fatabs',
			'carbonabs' => 'Carbonabs',
			'proteinabs' => 'Proteinabs',
			'type' => 'Type',
			'hh' => 'Hh',
			'mm' => 'Mm',
			'issport' => 'Issport',
			'chest' => 'Chest',
			'pelvis' => 'Pelvis',
			'biceps' => 'Biceps',
			'shin' => 'Shin',
			'neck' => 'Neck',
			'waist' => 'Waist',
			'forearm' => 'Forearm',
			'hip' => 'Hip',
			'userid' => 'Userid',
			'activeflag' => 'Activeflag',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('bodyweight',$this->bodyweight);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('daytime',$this->daytime);
		$criteria->compare('calorisity',$this->calorisity);
		$criteria->compare('calorie',$this->calorie);
		$criteria->compare('category',$this->category);
		$criteria->compare('dishweight',$this->dishweight);
		$criteria->compare('datetext',$this->datetext,true);
		$criteria->compare('datelong',$this->datelong);
		$criteria->compare('fat',$this->fat);
		$criteria->compare('carbon',$this->carbon);
		$criteria->compare('protein',$this->protein);
		$criteria->compare('fatabs',$this->fatabs);
		$criteria->compare('carbonabs',$this->carbonabs);
		$criteria->compare('proteinabs',$this->proteinabs);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('hh',$this->hh);
		$criteria->compare('mm',$this->mm);
		$criteria->compare('issport',$this->issport);
		$criteria->compare('chest',$this->chest);
		$criteria->compare('pelvis',$this->pelvis);
		$criteria->compare('biceps',$this->biceps);
		$criteria->compare('shin',$this->shin);
		$criteria->compare('neck',$this->neck);
		$criteria->compare('waist',$this->waist);
		$criteria->compare('forearm',$this->forearm);
		$criteria->compare('hip',$this->hip);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('activeflag',$this->activeflag);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}