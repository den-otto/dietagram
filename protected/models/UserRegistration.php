<?php

/**
 * This is the model class for table "user_registration".
 *
 * The followings are the available columns in table 'user_registration':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property integer $sex
 * @property double $weight
 * @property integer $hight
 * @property integer $age
 * @property integer $activitylvl
 */
class UserRegistration extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserRegistration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_registration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, password, name, sex, weight, hight, age, activitylvl', 'required'),
			array('sex, hight, age, activitylvl', 'numerical', 'integerOnly'=>true),
			array('weight', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, email, password, name, sex, weight, hight, age, activitylvl', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'name' => 'Name',
			'sex' => 'Sex',
			'weight' => 'Weight',
			'hight' => 'Hight',
			'age' => 'Age',
			'activitylvl' => 'Activitylvl',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('hight',$this->hight);
		$criteria->compare('age',$this->age);
		$criteria->compare('activitylvl',$this->activitylvl);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}