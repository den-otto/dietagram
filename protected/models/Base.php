<?php

/**
 * This is the model class for table "base".
 *
 * The followings are the available columns in table 'base':
 * @property integer $id
 * @property string $name
 * @property string $protein
 * @property string $fat
 * @property string $carbon
 * @property integer $caloric
 * @property string $category
 * @property string $type
 * @property integer $category_id
 * @property integer $isfull
 * @property integer $serving
 */
class Base extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Base the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'base';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('serving', 'required'),
			array('caloric, category_id, isfull, serving', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>74),
			array('protein, fat, carbon', 'length', 'max'=>4),
			array('category', 'length', 'max'=>25),
			array('type', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, protein, fat, carbon, caloric, category, type, category_id, isfull, serving', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'protein' => 'Protein',
			'fat' => 'Fat',
			'carbon' => 'Carbon',
			'caloric' => 'Caloric',
			'category' => 'Category',
			'type' => 'Type',
			'category_id' => 'Category',
			'isfull' => 'Isfull',
			'serving' => 'Serving',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('protein',$this->protein,true);
		$criteria->compare('fat',$this->fat,true);
		$criteria->compare('carbon',$this->carbon,true);
		$criteria->compare('caloric',$this->caloric);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('isfull',$this->isfull);
		$criteria->compare('serving',$this->serving);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}